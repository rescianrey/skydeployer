from django.apps import AppConfig


class PackageGeneratorConfig(AppConfig):
    name = 'package_generator'
