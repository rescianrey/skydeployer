import os
import pexpect
import subprocess

class PackageGenerator(object):
	"""
	Generates Salesforce deployment package given a Salesforce source repository.
	Outputs a zip file containing the modified files in between a start and end commits.
	"""
	SCM_EXT = '.git'

	def generate_package(repository_url, username, password, from_commit, to_commit,
			base_directory='source/', output_directory='result/', output_filename='package'):

		# Check source directory
		src_directory = None
		repository_name = repository_url.rplit('/', 1).replace(SCM_EXT, '')

		clone_repo = False
		if os.path.isdir(base_directory):
			_src_directory = os.path.join(base_directory, repository_name)
			if os.path.isdir(_src_directory):
				src_directory = _src_directory
				clone_repo = True
		else:
			os.mkdir(base_directory)

		if clone_repo:
			pass


