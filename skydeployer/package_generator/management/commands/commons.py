## Settings, common variables for management commands

REVISION_OUTPUT_FILENAME = 'revisions.txt'
MAPPING_FILE = 'mapping.txt'
PACKAGE_XML = 'package.xml'
DESTRUCTIVE_XML = 'destructiveChanges.xml'
PACKAGE_ZIP = 'package.zip'
TEMP_FOLDER = 'tmp'
SRC_FOLDER = 'src'
DEPLOYMENT_PKG_FOLDER = 'package'
EXCLUDED = [PACKAGE_XML, MAPPING_FILE]
META_TAIL = '-meta.xml'


SF_META_NS = 'http://soap.sforce.com/2006/04/metadata'
PKG_ROOT = 'Package'
PKG_TYPES = 'types'
PKG_MEMBERS = 'members'
PKG_NAME = 'name'
PKG_VERSION = 'version'

SPLITTER = ',\s+'