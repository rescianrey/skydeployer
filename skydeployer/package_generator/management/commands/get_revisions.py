from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

import os
import pexpect
import subprocess
import shutil

from .commons import *


class Command(BaseCommand):
    help = 'Get GIT Revisions'

    def add_arguments(self, parser):
        parser.add_argument('--repo', nargs=1)
        parser.add_argument('--password', nargs=1)
        parser.add_argument('--dir', nargs=1)

    def handle(self, *args, **options):

        session_folder = options['dir'][0]
        repository_url = options['repo'][0]

        session_dir = os.path.join(settings.TEMP_DIR, session_folder)
        if not os.path.isdir(session_dir):
            os.mkdir(session_dir)
        
        src_dir = os.path.join(session_dir, SRC_FOLDER)
        password = options['password'][0]

        if not os.path.isdir(src_dir):
            command = 'git clone %s %s' % (repository_url, SRC_FOLDER)
            process = pexpect.spawn(command, cwd=session_dir)
            process.expect('Password for .*:')

            process.sendline(password)
            process.expect(pexpect.EOF, timeout=240)
            
        else:
            command = 'git pull origin master'
            process = pexpect.spawn(command, cwd=src_dir)
            process.expect('Password for .*:')
            process.sendline(password)
            process.expect(pexpect.EOF, timeout=240)

        command = 'git log --pretty=oneline'
        p = subprocess.Popen(command, cwd=src_dir, stdout=subprocess.PIPE, shell=True)
        resp, err = p.communicate()

        out = open(os.path.join(session_dir, REVISION_OUTPUT_FILENAME), 'w')
        out.write(resp)
        out.close()