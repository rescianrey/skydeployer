from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

import os
import pexpect
import re
import shutil
import subprocess
from xml.dom import minidom
import xml.etree.ElementTree as ET

from .commons import *


class Command(BaseCommand):
    help = 'Clean directory'

    def add_arguments(self, parser):
        parser.add_argument('--dir', nargs=1)

    def handle(self, *args, **options):

        directory = options['dir'][0]

        command = 'rm -fr  %s' % directory
        process = pexpect.spawn(command, cwd=settings.TEMP_DIR)
        process.expect(pexpect.EOF, timeout=240)
        