from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

import os
import pexpect
import re
import shutil
import subprocess
from xml.dom import minidom
import xml.etree.ElementTree as ET

import string

from .commons import *


class Command(BaseCommand):
    help = 'Generates package'

    def add_arguments(self, parser):
        parser.add_argument('--from', nargs=1)
        parser.add_argument('--to', nargs=1)
        parser.add_argument('--dir', nargs=1)

    def handle(self, *args, **options):

        directory = options['dir'][0]
        to_commit_id = options['to'][0]
        from_commit_id = options['from'][0]

        session_directory = os.path.join(settings.TEMP_DIR, directory)

        if not os.path.isdir(session_directory):
            raise CommandError('Directory not found')

        src_dir = os.path.join(session_directory, SRC_FOLDER)

        command = 'git checkout %s' % to_commit_id
        process = pexpect.spawn(command, cwd=src_dir)
        process.expect(pexpect.EOF, timeout=240)
        
        command = 'git diff --name-only %s %s' % (from_commit_id, to_commit_id)
        p = subprocess.Popen(command, cwd=src_dir, stdout=subprocess.PIPE, shell=True)
        resp, err = p.communicate()

        modified_files = []
        for line in resp.split('\n'):
            if line and not line in EXCLUDED and not line.endswith(META_TAIL):
                modified_files.append(line)

        modified_files = list(set(modified_files))
        
        package_dir = os.path.join(session_directory, DEPLOYMENT_PKG_FOLDER)
        os.mkdir(package_dir)

        package_dict = {}
        destructive_dict = {}

        for mod_file in modified_files:
            try:
                folder, component_name = mod_file.rsplit('/', 1)
            except Exception, e:
                continue
            component_api_name = component_name.split('.')[0]

            mod_file_destination_folder = os.path.join(package_dir, folder)

            if not os.path.isdir(mod_file_destination_folder):
                os.mkdir(mod_file_destination_folder)

            folder_based = component_api_name.split('/', 1)
            if (len(folder_based) == 2 and
                    not os.path.isdir(os.path.join(mod_file_destination_folder, folder_based[0]))):
                os.mkdir(os.path.join(mod_file_destination_folder, folder_based[0]))

            mod_file_path = os.path.join(src_dir, mod_file)

            if os.path.isfile(mod_file_path):
                mod_file_destination_path = os.path.join(package_dir, mod_file)
                shutil.copyfile(mod_file_path, mod_file_destination_path)

                metafile = '%s%s' % (mod_file, META_TAIL)
                mod_file_meta_path = os.path.join(src_dir, metafile)

                if os.path.isfile(mod_file_meta_path):
                    mod_file_meta_destination_path = os.path.join(package_dir, metafile)
                    shutil.copyfile(mod_file_meta_path, mod_file_meta_destination_path)

                if not folder in package_dict:
                    package_dict[folder] = []
                package_dict[folder].append(component_api_name)
            else:
                if not folder in destructive_dict:
                    destructive_dict[folder] = []
                destructive_dict[folder].append(component_api_name)
            

        directory_map = {}
        map_path = os.path.join(src_dir, MAPPING_FILE)

        if not os.path.isfile(map_path):
            map_path = os.path.join(settings.STATIC_ROOT, 'package_generator', 'utility', MAPPING_FILE)

        for line in open(map_path):
            component, directory = re.compile(SPLITTER).split(line.strip())
            directory_map[directory] = component


        src_package = os.path.join(src_dir, PACKAGE_XML)
        version = ET.parse(src_package).find('{%s}%s' % (SF_META_NS, PKG_VERSION)).text

        root = ET.Element(PKG_ROOT)
        root.set('xmlns', SF_META_NS)

        for folder_name in sorted(package_dict.keys()):
            type_node = ET.SubElement(root, PKG_TYPES)

            for member in package_dict[folder_name]:
                member_node = ET.SubElement(type_node, PKG_MEMBERS)
                member_node.text = member

            name_node = ET.SubElement(type_node, PKG_NAME)
            name_node.text = directory_map[folder_name]

        version_node = ET.SubElement(root, PKG_VERSION)
        version_node.text = version

        package_xml = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ")

        output_file = os.path.join(package_dir, PACKAGE_XML)
        output = open(output_file, 'w')
        output.write(package_xml)
        output.close()

        if destructive_dict:
            root = ET.Element(PKG_ROOT)
            root.set('xmlns', SF_META_NS)

            for folder_name in sorted(destructive_dict.keys()):
                type_node = ET.SubElement(root, PKG_TYPES)

                for member in destructive_dict[folder_name]:
                    member_node = ET.SubElement(type_node, PKG_MEMBERS)
                    member_node.text = member

                name_node = ET.SubElement(type_node, PKG_NAME)
                name_node.text = directory_map[folder_name]

            version_node = ET.SubElement(root, PKG_VERSION)
            version_node.text = version

            package_xml = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ")

            output_file = os.path.join(package_dir, DESTRUCTIVE_XML)
            output = open(output_file, 'w')
            output.write(package_xml)
            output.close()