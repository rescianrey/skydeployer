from django.conf.urls import url
from django.views.generic import TemplateView, RedirectView

from .views import *


urlpatterns = [
    url(r'^$', PackageGenerator.as_view(), name='package-generator'),
    url(r'^repository-info/$', RepositoryInfo.as_view(), name='repository-info'),
    url(r'^clear-session/$', clear_session, name='clear-session'),
    url(r'^git/$', TemplateView.as_view(template_name='package_generator/package_generator_mock.html')),
    url(r'^generate/$', TemplateView.as_view(template_name='package_generator/generate.html')),
]
