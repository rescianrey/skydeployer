from django import forms


class RepositoryInfoForm(forms.Form):
    repository_url = forms.CharField(label='Repository HTTPS URL', max_length=200)
    password = forms.CharField(label='Repository Password', max_length=100, widget=forms.PasswordInput)


class PackageGeneratorForm(forms.Form):
    from_revision = forms.ChoiceField(label='From')
    to_revision = forms.ChoiceField(label='To')

    def __init__(self, from_options=[], to_options=[], *args, **kwargs):
        super(PackageGeneratorForm, self).__init__(*args, **kwargs)

        self.fields['from_revision'].choices = from_options
        self.fields['to_revision'].choices = to_options