from django.conf import settings
from django.core import management
from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template import RequestContext
from django.views.generic import View

from .forms import *
from .management.commands import commons

import os
import shutil
import StringIO
import zipfile

SESSION_KEY_REPO = 'repository_url'
## SESSION_KEY_USERNAME = 'username'
SESSION_KEY_PASSWORD = 'password'

class PackageGenerator(View):

    form_class = PackageGeneratorForm
    template = 'package_generator/package_generator.html'

    def get(self, request, *args, **kwargs):
        context = {}

        if not 'repository_url' in request.session:
            return redirect('repository-info')

        context['repository_name'] = request.session[SESSION_KEY_REPO].rsplit('/', 1)[1]

        management.call_command('get_revisions', 
                                '--repo', request.session[SESSION_KEY_REPO],
                                '--password', request.session[SESSION_KEY_PASSWORD],
                                '--dir', request.session.session_key
            )

        from_options, to_options = self.get_options(request.session.session_key)

        form = self.form_class(from_options, to_options)
        context['form'] = form

        return render_to_response(self.template, context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        context = {}

        context['repository_name'] = request.session[SESSION_KEY_REPO].rsplit('/', 1)[1]
        from_options, to_options = self.get_options(request.session.session_key)
        form = self.form_class(from_options, to_options, request.POST)

        if form.is_valid():
            from_commit_id = form.cleaned_data['from_revision']
            to_commit_id = form.cleaned_data['to_revision']

            session_dir = os.path.join(settings.TEMP_DIR, request.session.session_key)
            package_dir = os.path.join(session_dir, commons.DEPLOYMENT_PKG_FOLDER)

            if os.path.isdir(package_dir):
                shutil.rmtree(package_dir)

            management.call_command('generate_package', 
                            '--from', from_commit_id,
                            '--to', to_commit_id,
                            '--dir', request.session.session_key
                )
            
            s = StringIO.StringIO()

            # The zip compressor
            zf = zipfile.ZipFile(s, "w")
            zipdir(package_dir, zf, session_dir)
            zf.close()

            response = HttpResponse(s.getvalue(), content_type='application/x-zip-compressed')
            response['Content-Disposition'] = 'attachment; filename="%s"' % commons.PACKAGE_ZIP
            return response

        context.update({'form': form})
        return render_to_response(self.template, context, context_instance=RequestContext(request))

    def get_options(self, session_key):
        revisions = open(os.path.join(settings.TEMP_DIR, session_key, commons.REVISION_OUTPUT_FILENAME))

        from_options = [('', 'Select commit'), ]
        to_options = [('', 'Select commit'), ]

        for line in revisions:
            if line:
                revision_id, revision_name = line.split(' ', 1)
                from_options.append((revision_id, '%s' % revision_name))
                to_options.append((revision_id, '%s' % revision_name))
        return from_options, to_options


class RepositoryInfo(View):

    form_class = RepositoryInfoForm
    template = 'package_generator/repository_info.html'

    def get(self, request, *args, **kwargs):
        context = {}

        if SESSION_KEY_REPO in request.session:
            form = self.form_class(request.session)
        else:
            form = self.form_class()
            
        context.update({'form': form})

        return render_to_response(self.template, context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        context = {}

        form = self.form_class(request.POST)

        if form.is_valid():
            if (SESSION_KEY_REPO in request.session and
                    form.cleaned_data['repository_url'] != request.session[SESSION_KEY_REPO]):
                management.call_command('clean_directory',
                            '--dir', request.session.session_key
                )

            request.session[SESSION_KEY_REPO] = form.cleaned_data['repository_url']
            request.session[SESSION_KEY_PASSWORD] = form.cleaned_data['password']

            return redirect('package-generator')

        context.update({'form': form})

        return render_to_response(self.template, context, context_instance=RequestContext(request))


def clear_session(request):
    management.call_command('clean_directory',
                    '--dir', request.session.session_key
        )
    for key in request.session.keys():
        del request.session[key]
    return redirect('repository-info')


def zipdir(path, ziph, relative_to):
    for root, dirs, files in os.walk(path):
        for file in files:
            file_path = os.path.join(root, file)
            zip_path = os.path.relpath(file_path, relative_to)
            ziph.write(file_path, zip_path)